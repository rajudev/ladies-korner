import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private API_URL:string = environment.API_ENDPOINT+"auth/";
  constructor(
    private httpClient: HttpClient
  ) { }
  loginService(reqData){
		return this.httpClient.post(this.API_URL+"login", reqData)
			.pipe(map((response:Response) => {
				return response;
			}))
			.pipe(
				catchError((error) => {
					console.log('Error: ' + error);
					throw error;
				})
			)
  }
  signUpService(reqData){
	return this.httpClient.post(this.API_URL+"signup", reqData)
	.pipe(map((response:Response) => {
		return response;
	}))
	.pipe(
		catchError((error) => {
			console.log('Error: ' + error);
			throw error;
		})
	)
  }

  otpService(reqData){
	return this.httpClient.post(this.API_URL+"activate-account", reqData)
	.pipe(map((response:Response) => {
		return response;
	}))
	.pipe(
		catchError((error) => {
			console.log('Error: ' + error);
			throw error;
		})
	)
  }

}
