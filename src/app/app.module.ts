import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { HomeModule } from './modules/home/home.module';

import { AppComponent } from './app.component';
import { LadiesCornerHttpInterceptor } from './httpinterceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule,
    ToastrModule.forRoot(
      {
        timeOut: 2000,
        positionClass: 'toast-top-center',
        preventDuplicates: true,
      }
    )
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: LadiesCornerHttpInterceptor, 
      multi: true 
   },
   CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
