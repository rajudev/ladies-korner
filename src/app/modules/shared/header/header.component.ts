import { Component, OnInit, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthService } from '../../../_services/auth/auth.service';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('headerModal', null) headerModal: ModalDirective;
  isLoading = false;
  loginForm: FormGroup;
  islogin: boolean = true;
  issignup : boolean = false;
  forgotpwd : boolean = false;
  otpShow: boolean = false;
  account:boolean = false;
  signupDetails:any = {
      name:null,
      email:null,
      pwd:null,
      cnf_pwd:null,
      phone:null
  }
  otpDetails:any ={
    email:null,
    otp:null
  }
  signupMail:null;

  
  constructor(
    private spinnerService: Ng4LoadingSpinnerService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private cookieService: CookieService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.createForm();
    this.isloggedin();
  }
  showheaderModal(): void {
    this.headerModal.show();
  }
  closeHeaderModal(){
    this.headerModal.hide();
    this.showForm('login');
  }

  login() {
    this.isLoading = true;
    this.spinnerService.show();
    const reqParam = {
      email: this.loginForm.value.email,
      pwd: this.loginForm.value.password
    };
    if(reqParam.email == ''){
      this.toastr.error('Email is required', 'error!');
    } else if (reqParam.pwd == ''){
      this.toastr.error('Password is required', 'error!');
    } else {
      this.authService.loginService(reqParam)
      .pipe(finalize(() => {
        this.loginForm.markAsPristine();
        this.isLoading = false;
      }))
      .subscribe(resData => {
        this.spinnerService.hide();
        let loginResponse:any = resData;
        if(loginResponse.error){
          console.log('error login',loginResponse.error)
          this.toastr.error(loginResponse.error, 'Error!');
        }else{
          this.cookieService.set( 'token', loginResponse.result.token );
          this.toastr.success('Login success', 'success!');
          this.account = true;
          this.headerModal.hide();
          //this.router.navigate(['course-list']);
        }
      }, error => {
        this.spinnerService.hide();
        this.toastr.error('Login error');
        console.log(error);
      });
    }
  }

  registerUser(){
    console.log('clicked')
    if(!this.signupDetails.name){
      this.toastr.warning('Name is required!','',{
        progressBar: true,
      });
    }
    else if(!this.signupDetails.email){
      this.toastr.warning('Email is required!','',{
        progressBar: true,
      });
    }else if(!this.signupDetails.phone){
      this.toastr.warning('Phone Number is required!','',{
        progressBar: true,
      });
    }else if(this.signupDetails.cnf_pwd !== this.signupDetails.pwd){
      this.toastr.warning('Password not matched','',{
        progressBar: true,
      });
    } else {
      this.spinnerService.show();

      let reqData = {
        name : this.signupDetails.name,
        email : this.signupDetails.email,
        pwd : this.signupDetails.pwd,
        phone : this.signupDetails.phone
      };

      this.authService.signUpService(reqData)
      .subscribe(resData => {
        this.spinnerService.hide();
        let signRes:any = resData;
        console.log(signRes)
        if(signRes.result){
          this.signupMail = reqData.email;
          console.log('signup', this.signupMail)
          this.toastr.success(signRes.result);
          this.issignup = false;
          this.otpShow = true;
        } else {
          this.toastr.error(signRes.error);
        }

       }, error => {
        this.spinnerService.hide();
        console.log(error);
      });
    }
  }

  otpActivate(){
    if(!this.otpDetails.otp){
      this.toastr.warning('OTP is required!','',{
        progressBar: true,
      });
    } else {
      this.spinnerService.show();

      let reqData = {
        email : this.signupMail,
        otp : this.otpDetails.otp,
      };
      console.log('reqdata', reqData.email)

      this.authService.otpService(reqData)
      .subscribe(resData => {
        this.spinnerService.hide();
        let otpRes:any = resData;
        console.log(otpRes)
        if(otpRes.status == true){
          this.toastr.success('Account Activated Successfully');
          this.otpShow = false;
          this.islogin = true;
          this.signupDetails = {};
        } else {
          this.toastr.error(otpRes.error);
        }

       }, error => {
        this.spinnerService.hide();
        console.log(error);
      });
    }
  }


  isloggedin(){
    if(this.cookieService.check('token')){
      this.account = true;
    }else{
      this.account = false;
    }
  }
  logout(){
    this.cookieService.delete('token');
    window.location.reload();
    this.router.navigate(['/']);
  }
  private createForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
  }

  showForm(formname){
    if(formname == 'login'){
      this.issignup = false;
      this.forgotpwd = false;
      this.otpShow = false;
      this.islogin = true;
    }else if(formname == 'signup'){
      this.islogin = false;
      this.forgotpwd = false;
      this.issignup = true;
    } else if (formname == 'fgpwd'){
      this.islogin = false;
      this.issignup = false;
      this.forgotpwd = true
    }
  }



}
