import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AuthService } from '../../_services/auth/auth.service';
import { FormsModule } from '@angular/forms';



import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from "ngx-bootstrap";
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    HeaderComponent, 
    FooterComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule
  ],
  exports:[
    ModalModule,
    HeaderComponent, 
    FooterComponent
  ],
  providers:[
    AuthService
    
   
  ]
})
export class SharedModule { }
