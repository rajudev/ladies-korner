import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import { from, Observable} from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable()

export class LadiesCornerHttpInterceptor implements HttpInterceptor {
    constructor(private cookieService: CookieService) { }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return from(this.handleAccess(request, next));
    }

    private async handleAccess(request: HttpRequest<any>, next: HttpHandler):
          Promise<HttpEvent<any>> {
        const token = this.cookieService.check('token') ? this.cookieService.get('token') : "";
        let changedRequest = request;
        // HttpHeader object immutable - copy values
        const headerSettings: {[name: string]: string | string[]; } = {};
    
        let isEncTypeheader:boolean = false;
        for (const key of request.headers.keys()) {
            headerSettings[key] = request.headers.getAll(key);
            if(key === 'enctype') {
                isEncTypeheader = true;
            }
        }
        
        if (token) {
            headerSettings['Authorization'] = 'Bearer ' + token;
        }

        if(isEncTypeheader === false) {
            headerSettings['Content-Type'] = 'application/json';
        }

        const newHeader = new HttpHeaders(headerSettings);

        changedRequest = request.clone(
          {headers: newHeader}
        );
        
        return next.handle(changedRequest).toPromise();
    }
}